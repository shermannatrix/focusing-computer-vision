# Obtain an image from the camera along with the exact metadata that describes
# that image.

from picamera2 import Picamera2, Preview
import time

picam2 = Picamera2()
picam2.start_preview(Preview.QTGL)

preview_config = picam2.preview_configuration()
picam2.configure(preview_config)

picam2.start()
time.sleep(2)

request = picam2.capture_request()
image = request.make_image("main")
metadata = request.get_metadata()
request.release()

image.show()
print(metadata)